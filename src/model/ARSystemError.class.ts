import { ARErrorResponse } from '../interface';

export class ARSystemError extends Error {
  constructor(private error: ARErrorResponse) {
    super();
    this.message = this.toString();
  }
  public get Type() {
    return this.error.messageType;
  }
  public get Message() {
    return this.error.messageText;
  }
  public get Code() {
    return this.error.messageNumber;
  }
  public get Details() {
    return this.error.messageAppendedText;
  }
  public toString() {
    return `ARSystemError[${this.Code}]: ${this.Message} - ${this.Details}`;
  }
}
