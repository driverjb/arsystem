import { ARResponse, ARSystemConfiguration, ARSystemRequestOptions } from '../interface';
import { ARSystemError } from './ARSystemError.class';
import https from 'https';

export type Method = 'get' | 'post' | 'put' | 'delete';

const ERROR = {
  usernameRequired: new Error(`Username is required if no service account is provided`),
  passwordRequired: new Error(`Password is required if no service account is provided`),
  invalidServiceAccount: new Error(
    `An invalid service account configuration was provided. User and password must be provided.`
  ),
  impossibleCredError: new Error(
    `This error should never happen, but if it does you need to provide either a service account or on-demand username and password to acquire a token.`
  ),
  failedTokenAcquisition: new Error(`Failed to acquire JWT`)
};

export class ARSystemAPI {
  constructor(private config: ARSystemConfiguration) {}
  private getBaseOptions(path: string, method: Method, token?: string): https.RequestOptions {
    const options: https.RequestOptions = {
      host: this.config.api.hostname,
      path,
      method: method.toUpperCase(),
      port: this.config.api.port
    };
    options.headers = {};
    if (token) options.headers['Authorization'] = `AR-JWT ${token}`;
    return options;
  }
  /**
   * Clean way to decide between a service account and provided creds
   * @param username
   * @param password
   * @returns
   */
  private determineCreds(username?: string, password?: string) {
    if (username && password) return [username, password];
    if (this.config.serviceAccount) {
      if (this.config.serviceAccount.user && this.config.serviceAccount.password)
        return [this.config.serviceAccount.user, this.config.serviceAccount.password];
      throw ERROR.invalidServiceAccount;
    }
    if (!username) throw ERROR.usernameRequired;
    if (!password) throw ERROR.passwordRequired;
    throw ERROR.impossibleCredError;
  }
  /**
   * Get the expiration Date of the token
   * @param token
   * @returns a standard JavaScript date object
   */
  public getTokenExpiration(token: string) {
    const [header, payload, signature] = token.split('.');
    const { exp } = JSON.parse(Buffer.from(payload, 'base64').toString());
    return new Date(exp * 1000);
  }
  /**
   * Compare the value of the token's expiration date with now to determine if the token is expired
   * @param token
   * @returns
   */
  public isTokenExpired(token: string) {
    const expiration = this.getTokenExpiration(token);
    const now = Date.now();
    return now.valueOf() > expiration.valueOf();
  }
  /**
   * Acquire a JSON Web Token (JWT) using the provided credentials
   * @param username
   * @param password
   * @returns A JSON Web Token
   */
  public getToken(username?: string, password?: string) {
    const [user, pass] = this.determineCreds(username, password);
    const options = this.getBaseOptions('/api/jwt/login', 'post');
    options.headers!['Content-Type'] = 'application/x-www-form-urlencoded';
    options.headers!['authString'] = this.config.api.authString;
    const params = new URLSearchParams();
    params.append('username', user);
    params.append('password', pass);
    return new Promise<string>((resolve, reject) => {
      const req = https.request(options, (res) => {
        const data: string[] = [];
        res.on('error', reject);
        res.on('data', (chunk) => data.push(chunk));
        res.on('end', () => {
          let final = data.join('');
          if (final.includes(`ERROR`)) return reject(ERROR.failedTokenAcquisition);
          return resolve(data.join(''));
        });
      });
      req.write(params.toString());
      req.end();
    });
  }
  /**
   * Invalidate the provided JSON Web Token (JWT)
   * @param token The JWT to invalidate
   * @returns
   */
  public releaseToken(token: string) {
    const options = this.getBaseOptions('/api/jwt/logout', 'post', token);
    return new Promise<void>((resolve, reject) => {
      https
        .request(options, (res) => {
          const data: string[] = [];
          res.on('data', (chunk) => data.push(chunk));
          res.on('end', resolve);
          res.on('error', reject);
        })
        .end();
    });
  }
  /**
   * Make requests to the BMC Remedy ARSystem REST API
   * @param token The token acquired with valid credentials
   * @param formName The form with ARSystem that is to be queried
   * @param method The HTTP method to be used by the query
   * @param options Options to augment the request
   * @returns
   */
  public request<T = any>(
    token: string,
    formName: string,
    method: Method,
    options: ARSystemRequestOptions = {}
  ) {
    const urlParams = new URLSearchParams();
    if (options.query) urlParams.set('q', options.query);
    if (options.fields) urlParams.set('fields', `values(${options.fields.join(',')})`);
    const path = `/api/arsys/v1/entry/${encodeURIComponent(formName)}?${urlParams.toString()}`;
    const requestOptions = this.getBaseOptions(path, method, token);
    requestOptions.headers!['Content-Type'] = 'application/json';
    return new Promise((resolve, reject) => {
      const req = https.request(requestOptions, (res) => {
        const data: string[] = [];
        res.on('data', (chunk) => data.push(chunk));
        res.on('end', () => {
          try {
            const json = JSON.parse(data.join(''));
            if (Array.isArray(json)) return reject(new ARSystemError(json[0]));
            return resolve(json as ARResponse<T>);
          } catch (err) {
            return reject(err);
          }
        });
        res.on('error', reject);
      });
      if (method !== 'get') req.write(options.body ? JSON.stringify(options.body) : '');
      req.end();
    });
  }
}
