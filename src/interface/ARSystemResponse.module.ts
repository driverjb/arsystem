export interface ARSystemLink {
  [key: string]: { href: string }[];
}

export interface ARResponse<T = any> {
  entries: {
    values: T;
    _links: ARSystemLink;
  };
  _links: ARSystemLink;
}

export interface ARErrorResponse {
  messageType: 'ERROR';
  messageText: string;
  messageNumber: number;
  messageAppendedText: string;
}
