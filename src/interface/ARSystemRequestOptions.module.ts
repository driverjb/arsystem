export interface ARSystemRequestOptions {
  /** A JSON body that will be included with the request (this is for POST/PUT/DELETE requests) */
  body?: any;
  /** Allows you to limit which fields you'd like returned in the response */
  fields?: string[];
  /** An ARSystem query string to limit the results of a search or filter for updates/deletions */
  query?: string;
}
