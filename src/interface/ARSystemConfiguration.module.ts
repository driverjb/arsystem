export interface ARSystemConfiguration {
  api: {
    hostname: string;
    port: number;
    authString: string;
  };
  serviceAccount?: {
    user: string;
    password: string;
  };
}
