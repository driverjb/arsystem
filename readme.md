# BMC Remedy ARSystem

The is an open source implementation of the BCM Remedy Action Request System version 9.1 as described in their publicly available documentation located here: https://docs.bmc.com/docs/ars91/en/developing-an-api-program-609071507.html. Since the ARSystem environment can vary from company to company with the ability to create custom forms, I have opted to only implement basic functionality. However, this will provide a nice head start with ARSystem API development.

# Usage

The examples are written in TypeScript:

```typescript
import { ARSystemAPI, ARSystemError } from 'arsystem';

// The configuration. Please don't store passwords in code. This should all be loaded by a configuration file.
const arConfig = {
  api: {
    hostname: 'myarsystem.instance.somewhere.com',
    port: 9443,
    authString: 'myAuthString'
  },
  serviceAccount: {
    user: 'someServiceUser',
    password: 'someServicePassword'
  }
};

// Create an instance of the ARSystem API
const api = new ARSystemAPI(arConfig);

/**
 * Get a token and make a request. It's up to you and your application's requirements if it is
 * appropriate to use a service account or get tokens for each user interacting with the API. Also,
 * ARSystem tokens have an expiration time by default of 1 hour. It's also up to your design whether
 * you want to juggle tokens and expirations or if you just want to get a token, make a bunch of
 * requests and then release the token each time.
 *
 * Because of the one hour default, the easiest route is to use a service account so you always have
 * the credentials on hand to acquire new tokens. Then just acquire a token for a batch of requests
 * and release the token when the requests are compelte.
 */
api.getToken().then(async (token) => {
  const formName = 'WOI:WorkOrder';
  const query = `'Work Order ID' = "WO0000000123456"`;
  const fields = ['Summary', 'Work Order Type', 'Priority'];
  const method = 'get';
  await api
    .request(token, formName, method, { query, fields })
    .then((result) => {
      console.log(JSON.stringify(result, null, 2));
    })
    .catch((err: ARSystemError) => console.log(err.toString()));
  await api.releaseToken(token).then(() => console.log('Token released'));
});

/**
 * Example of getting a user specific token. This will return a token in the context of the given
 * username and password instead of the service account.
 */
api.getToken('username', 'password').then(async (token) => {
  const formName = 'WOI:WorkOrder';
  const query = `'Work Order ID' = "WO0000000123456"`;
  const fields = ['Summary', 'Work Order Type', 'Priority'];
  const method = 'get';
  await api
    .request(token, formName, method, { query, fields })
    .then((result) => {
      console.log(JSON.stringify(result, null, 2));
    })
    .catch((err: ARSystemError) => console.log(err.toString()));
  await api.releaseToken(token).then(() => console.log('Token released'));
});
```
